package com.elielmartinez.naturalfoodsstore.repository;

import com.elielmartinez.naturalfoodsstore.model.Product;

import java.util.List;

import io.reactivex.Observable;

/**
 * A simple repository class with static methods to handle our API, and maybe in a future
 * combine it with a caching strategy as well as a Persistence strategy
 */
public class FoodsRepository {

    /**
     * Retrieves a List of {@link Product}
     * @param page the page to be requested for pagination
     * @return a List of {@link Product}
     */
    public static Observable<List<Product>> getProducts(int page) {
        return FoodsService.getInstance().getApi().getProducts(page);
    }

}
