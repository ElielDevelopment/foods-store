package com.elielmartinez.naturalfoodsstore.repository;

import com.elielmartinez.naturalfoodsstore.model.Product;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class FoodsService {
    private static FoodsService service = null;
    private FoodsApi foodsApi;

    private static Retrofit retrofit = null;

    public static FoodsService getInstance() {
        if (service == null) {
            service = new FoodsService();
        }

        return service;
    }

    private FoodsService() {
        foodsApi = getClient().create(FoodsApi.class);
    }

    public FoodsApi getApi() {
        return foodsApi;
    }

    private static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl("https://stark-atoll-33661.herokuapp.com/")
                    .build();
        }
        return retrofit;
    }

    public interface FoodsApi {
        @GET("products.php")
        Observable<List<Product>> getProducts(@Query("page") int page);
    }
}
