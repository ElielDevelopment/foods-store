package com.elielmartinez.naturalfoodsstore.products.view.contract;

import com.elielmartinez.naturalfoodsstore.base.contracts.ViewContract;
import com.elielmartinez.naturalfoodsstore.model.Product;

import java.util.List;

public interface ProductListViewContract extends ViewContract{

    /**
     * Sets the Page Size to the view for Scrolling
     * @param pageSize
     */
    void setPageSize(int pageSize);

    /**
     * Renders a list of given products
     * @param productList a list of {@link Product} objects
     */
    void renderProductList(List<Product> productList);

    /**
     * Shows a {@link android.widget.ProgressBar} at the footer
     * <p/>
     * of the {@link android.support.v7.widget.RecyclerView}
     */
    void showLoadingFooter();

    /**
     * Shows the {@link android.widget.ProgressBar} at the footer
     * <p/>
     * of the {@link android.support.v7.widget.RecyclerView}
     */
    void dismissLoadingFooter();

}
