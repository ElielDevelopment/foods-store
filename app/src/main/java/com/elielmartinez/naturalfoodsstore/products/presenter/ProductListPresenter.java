package com.elielmartinez.naturalfoodsstore.products.presenter;

import android.util.Log;

import com.elielmartinez.naturalfoodsstore.base.BasePresenter;
import com.elielmartinez.naturalfoodsstore.model.Error;
import com.elielmartinez.naturalfoodsstore.model.Product;
import com.elielmartinez.naturalfoodsstore.products.presenter.contract.ProductListPresenterContract;
import com.elielmartinez.naturalfoodsstore.products.view.contract.ProductListViewContract;
import com.elielmartinez.naturalfoodsstore.repository.FoodsRepository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Presenter to handle the business logic for our unique Product List Screen.
 */
public class ProductListPresenter extends BasePresenter<ProductListViewContract> implements ProductListPresenterContract {

    private static final String TAG = ProductListPresenter.class.getSimpleName();
    private final static int PAGE_SIZE = 10;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @Override
    public void onViewStarted() {
        super.onViewStarted();
        getViewOrThrow().setPageSize(PAGE_SIZE);
        if (shouldRefreshProductList()) {
            refreshProductListIfNeeded(1);
        }
    }

    @Override
    public void refreshProductListIfNeeded(final int page) {
        if (shouldRefreshProductList()) {
            if (page == 1) {
                getViewOrThrow().showLoadingIndicator();
            } else {
                getViewOrThrow().dismissLoadingFooter();
            }

            isLoading = true;



            addDisposable(FoodsRepository.getProducts(page)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<List<Product>>() {
                        @Override
                        public void onNext(List<Product> productList) {

                            if (page == 1) {
                                getViewOrThrow().dismissLoadingIndicator();
                            }

                            getViewOrThrow().showLoadingFooter();

                            isLoading = false;

                            if (!productList.isEmpty()) {
                                getViewOrThrow().renderProductList(productList);
                            } else {
                                isLastPage = true;
                                getViewOrThrow().dismissLoadingFooter();
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            getViewOrThrow().showErrorMessage(new Error());
                        }

                        @Override
                        public void onComplete() {
                        }
                    }));
        }

    }

    @Override
    public boolean isLastPage() {
        return isLastPage;
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    private boolean shouldRefreshProductList() {
        return !isLastPage;

    }
}
