package com.elielmartinez.naturalfoodsstore.products.view;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.elielmartinez.naturalfoodsstore.R;
import com.elielmartinez.naturalfoodsstore.base.BaseActivity;
import com.elielmartinez.naturalfoodsstore.model.Product;
import com.elielmartinez.naturalfoodsstore.products.presenter.ProductListPresenter;
import com.elielmartinez.naturalfoodsstore.products.view.adapter.ProductsAdapter;
import com.elielmartinez.naturalfoodsstore.products.view.contract.ProductListViewContract;
import com.elielmartinez.naturalfoodsstore.products.view.utils.EndlessRecyclerViewScrollListener;

import java.util.List;

public class ProductListActivity extends BaseActivity<ProductListPresenter> implements ProductListViewContract {

    private static final String TAG = ProductListActivity.class.getSimpleName();

    private RecyclerView productsList;
    private ProgressBar progressBar;
    private ProductsAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener listener;
    private SearchView searchView;

    public final static String LIST_STATE_KEY = "recycler_list_state";
    Parcelable listState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        productsList = findViewById(R.id.product_list);
        progressBar = findViewById(R.id.main_progress);
        adapter = new ProductsAdapter(this);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        productsList.setLayoutManager(linearLayoutManager);
        productsList.setItemAnimator(new DefaultItemAnimator());
        productsList.setAdapter(adapter);

        listener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                presenter.refreshProductListIfNeeded(page);
            }

            @Override
            public boolean isLastPage() {
                return presenter.isLastPage();
            }

            @Override
            public boolean isLoading() {
                return presenter.isLoading();
            }
        };

        productsList.addOnScrollListener(listener);
    }

    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        listState = linearLayoutManager.onSaveInstanceState();
        state.putParcelable(LIST_STATE_KEY, listState);
    }

    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        if(state != null)
            listState = state.getParcelable(LIST_STATE_KEY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (listState != null) {
            linearLayoutManager.onRestoreInstanceState(listState);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected ProductListPresenter createPresenter() {
        presenter = (ProductListPresenter) getLastCustomNonConfigurationInstance();
        if (presenter == null) {
            presenter = new ProductListPresenter();
        }

        return presenter;
    }

    @Override
    public void setPageSize(int pageSize) {
        listener.setPageSize(pageSize);
    }

    @Override
    public void renderProductList(List<Product> productList) {
        Log.d(TAG, productList.toString());
        adapter.addAll(productList);
    }

    @Override
    public void showLoadingFooter() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                adapter.addLoadingFooter();
            }
        });
    }

    @Override
    public void dismissLoadingFooter() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                adapter.removeLoadingFooter();
            }
        });
    }

    @Override
    public void showLoadingIndicator() {
        super.showLoadingIndicator();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoadingIndicator() {
        super.dismissLoadingIndicator();
        progressBar.setVisibility(View.GONE);
    }
}
