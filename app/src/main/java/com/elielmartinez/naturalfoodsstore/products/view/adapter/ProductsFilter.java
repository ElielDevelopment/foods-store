package com.elielmartinez.naturalfoodsstore.products.view.adapter;

import android.widget.Filter;

import com.elielmartinez.naturalfoodsstore.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductsFilter extends Filter {

    private FilterResults results;

    List<Product> productList;

    private OnPublishResultsListener publishResultsListener;

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void setPublishResultsListener(OnPublishResultsListener publishResultsListener) {
        this.publishResultsListener = publishResultsListener;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        String constraintStr = constraint.toString();

        results = new FilterResults();
        if (constraintStr.isEmpty()) {
            results.values = productList;
            results.count = productList.size();
        } else {
            ArrayList<Product> filteredProducts = new ArrayList<>();

            for (Product product : productList) {
                if (isQueryMatch(product.getTitle(), constraintStr)) {
                    filteredProducts.add(product);
                }
            }

            results.values = filteredProducts;
            results.count = filteredProducts.size();
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults filterResults) {
        publishResultsListener.publishResults((ArrayList<Product>) results.values);
    }

    public interface OnPublishResultsListener {
        void publishResults(List<Product> filteredProducts);
    }

    /**
     * Checks if the given title contains the given query
     * @param title
     * @param query
     * @return
     */
    private boolean isQueryMatch(String title, String query) {
        return title != null && query != null && title.toLowerCase().contains(query.toLowerCase());
    }
}
