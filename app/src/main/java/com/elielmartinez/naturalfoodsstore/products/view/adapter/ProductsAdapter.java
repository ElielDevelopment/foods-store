package com.elielmartinez.naturalfoodsstore.products.view.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.elielmartinez.naturalfoodsstore.R;
import com.elielmartinez.naturalfoodsstore.base.BaseViewHolder;
import com.elielmartinez.naturalfoodsstore.model.Product;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<BaseViewHolder> implements Filterable, ProductsFilter.OnPublishResultsListener {
    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_LOADING = 1;

    private boolean isLoaderVisible = false;

    private List<Product> data;
    private Context context;

    private ProductsFilter productsFilter;


    public ProductsAdapter(Context context) {
        data = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BaseViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                View viewItem = inflater.inflate(R.layout.product_list_item, parent, false);
                viewHolder = new ProductViewHolder(viewItem);
                break;
            default:
                View viewLoading = inflater.inflate(R.layout.product_list_item_progress, parent, false);
                viewHolder = new LoadingViewHolder(viewLoading);
                break;
        }
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder viewHolder, int position) {
        viewHolder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoaderVisible) {
            return position == data.size() - 1 ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        } else {
            return VIEW_TYPE_ITEM;
        }

    }

    @Override
    public Filter getFilter() {
        if (productsFilter == null) {
            productsFilter = new ProductsFilter();
            productsFilter.setPublishResultsListener(this);
            productsFilter.setProductList(data);
        }

        return productsFilter;
    }

    @Override
    public void publishResults(List<Product> filteredProducts) {
        isLoaderVisible = false;
        data = filteredProducts;
        notifyDataSetChanged();
    }

    /**
     * ViewHolder that represents a {@link Product}
     */
    protected class ProductViewHolder extends BaseViewHolder {
        private TextView title;
        private TextView price;
        private ImageView image;

        ProductViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.product_title);
            price = itemView.findViewById(R.id.product_price);
            image = itemView.findViewById(R.id.product_image);
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            Product product = data.get(position);
            title.setText(product.getTitle() != null ? product.getTitle() : context.getText(R.string.title_place_holder));
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            String priceString = formatter.format(product.getPrice());
            price.setText(priceString);

            float dip = 75f;
            Resources r = context.getResources();
            float px = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    dip,
                    r.getDisplayMetrics()
            );

            int imageSizeInPixels = Math.round(px);

            if (product.getImage() != null) {

                Picasso.with(context)
                        .load(product.getImage())
                        .resize(imageSizeInPixels, imageSizeInPixels)
                        .centerCrop()
                        .placeholder(R.color.colorPrimaryLight)
                        .error(R.mipmap.ic_launcher_round)
                        .into(image);
                return;
            }

            image.setBackgroundResource(R.mipmap.ic_launcher_round);
        }
    }

    /**
     * ViewHolder that represents a Loading indicator
     */
    protected class LoadingViewHolder extends BaseViewHolder {
        private ProgressBar progressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);

            progressBar = itemView.findViewById(R.id.item_progress);
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Adds a product to our data set
     * @param product
     */
    public void add(Product product) {
        data.add(product);
        notifyItemInserted(data.size() - 1);
    }

    /**
     * Adds a List of {@link Product} removing the duplicated ones
     * @param newProducts
     */
    public void addAll(List<Product> newProducts) {
        LinkedHashSet<Product> currentSet = new LinkedHashSet<>(data);

        LinkedHashSet<Product> setToAdd = new LinkedHashSet<>(newProducts);

        setToAdd.removeAll(currentSet);

        if (setToAdd.size() > 0) {
            for (Product product : setToAdd) {
                add(product);
            }
        }
    }

    /**
     * Remocves a {@link Product} from our data set
     * @param product
     */
    private void remove(Product product) {
        int position = data.indexOf(product);
        if (position > -1) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    /**
     * Adds a Loading indicator when we are requesting a new page
     */
    public void addLoadingFooter() {
        isLoaderVisible = true;
        add(new Product());
    }

    /**
     * Removes a Loading indicator when we finished the request of a new page
     */
    public void removeLoadingFooter() {
        isLoaderVisible = false;
        int position = data.size() - 1;
        Product product = getItem(position);
        if (product != null) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    /**
     * Removes all the items from our data set
     */
    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    /**
     * Gets the {@link Product} instance at the given position
     * @param position
     * @return
     */
    private Product getItem(int position) {
        return data.get(position);
    }
}
