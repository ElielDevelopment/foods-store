package com.elielmartinez.naturalfoodsstore.products.presenter.contract;

import com.elielmartinez.naturalfoodsstore.base.contracts.PresenterContract;
import com.elielmartinez.naturalfoodsstore.products.view.contract.ProductListViewContract;

public interface ProductListPresenterContract extends PresenterContract<ProductListViewContract> {

    /**
     * Retrieves the Product List according to the given page
     */
    void refreshProductListIfNeeded(int page);

    /**
     * True if we have reached the last page of our Product List
     * @return
     */
    boolean isLastPage();

    /**
     * True if we are loading more Products
     * @return
     */
    boolean isLoading();

}
