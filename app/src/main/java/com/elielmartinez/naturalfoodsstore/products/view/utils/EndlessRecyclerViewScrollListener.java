package com.elielmartinez.naturalfoodsstore.products.view.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * {@link RecyclerView.OnScrollListener} which allows us to handle endless Scrolling
 * <p/>
 * for a {@link RecyclerView}
 * <p/>
 * Based on the solution found in:
 * https://androidwave.com/pagination-in-recyclerview/
 *
 */
public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager layoutManager;

    private int currentPage = 1;
    private int pageSize = 0;

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Supporting only LinearLayoutManager for now.
     *
     * @param layoutManager
     */
    public EndlessRecyclerViewScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= pageSize) {
                currentPage++;
                onLoadMore(currentPage);
            }
        }

    }

    /**
     * Used to handle the load more items scroll event
     * @param page The page to be requested
     */
    public abstract void onLoadMore(int page);

    /**
     * True if we have reached the last page
     * @return
     */
    public abstract boolean isLastPage();

    /**
     * True if we are loading more items
     * @return
     */
    public abstract boolean isLoading();

}

