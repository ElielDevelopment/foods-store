package com.elielmartinez.naturalfoodsstore.base.contracts;

/**
 * Contract that a Valid Presenter must implement
 * @param <V> a valid View which implements a {@link ViewContract}
 */
public interface PresenterContract<V extends ViewContract> {

    /**
     * Handles (if needed) the onStart View Lifecycle event.
     *
     */
    void onViewStarted();

    /**
     * Handles (if needed) the onPause View Lifecycle event.
     *
     */
    void onViewPaused();

    /**
     * Handles (if needed) the onResume View Lifecycle event.
     *
     */
    void onViewResumed();

    /**
     * Handles (if needed) the onStop View Lifecycle event.
     *
     */
    void onViewStopped();

    /**
     * Attaches the {@link V} view to the presenter
     *
     * @param view the view to attach
     */
    void attachView(V view);

    /**
     * Detaches the attached {@link V} view from the presenter.
     */
    void detachView();
}
