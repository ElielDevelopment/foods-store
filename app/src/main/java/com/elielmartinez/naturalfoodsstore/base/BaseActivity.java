package com.elielmartinez.naturalfoodsstore.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.elielmartinez.naturalfoodsstore.base.contracts.PresenterContract;
import com.elielmartinez.naturalfoodsstore.base.contracts.ViewContract;
import com.elielmartinez.naturalfoodsstore.model.Error;

public abstract class BaseActivity<P extends PresenterContract> extends AppCompatActivity implements ViewContract {

    private static final String TAG = "LifeCycle";
    protected P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        presenter = createPresenter();
        //noinspection unchecked
        presenter.attachView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        presenter.onViewStarted();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        presenter.onViewResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        presenter.onViewPaused();

        if (isFinishing()) {
            presenter.detachView();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        presenter.onViewStopped();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        presenter.detachView();
        super.onDestroy();
    }

    protected abstract P createPresenter();

    @Override
    public void showLoadingIndicator() {
        // No default behavior
    }

    @Override
    public void dismissLoadingIndicator() {
        // No default behavior
    }

    @Override
    public void showErrorMessage(Error error) {
        // No default behavior
    }

    @Override
    public void dismissErrorMessage() {
        // No default behavior
    }
}
