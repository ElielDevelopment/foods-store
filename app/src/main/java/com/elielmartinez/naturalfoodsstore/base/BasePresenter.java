package com.elielmartinez.naturalfoodsstore.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.elielmartinez.naturalfoodsstore.base.contracts.PresenterContract;
import com.elielmartinez.naturalfoodsstore.base.contracts.ViewContract;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Base Presenter from  which every Presenter should extend
 */
public class BasePresenter<V extends ViewContract> implements PresenterContract<V> {
    @Nullable
    private WeakReference<V> view;

    private CompositeDisposable compositeDisposable;

    @NonNull
    public V getViewOrThrow() {
        if (view == null) {
            throw new IllegalStateException("View hasn't been attached to presenter");
        }
        return view.get();
    }

    @Override
    public void onViewStarted() {
        // No default behavior
    }

    @Override
    public void onViewPaused() {
        // No default behavior
    }

    @Override
    public void onViewResumed() {
        // No default behavior
    }

    @Override
    public void onViewStopped() {
        // No default behavior
    }

    @Override
    public void attachView(V view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        this.view = null;
        onViewDetached();
    }

    /**
     * Triggered after the view has been detached and should no longer be used
     * <p>
     * Child classes can override this for any specific cleanup
     */
    private void onViewDetached() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
    }

    /**
     * <p>
     * Use to manage RxJava disposables and keep them in sync with the View Lifecycle
     *
     * @param disposable
     */
    protected final void addDisposable(Disposable disposable) {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        compositeDisposable.add(disposable);
    }
}
