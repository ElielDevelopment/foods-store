package com.elielmartinez.naturalfoodsstore.base.contracts;

import com.elielmartinez.naturalfoodsstore.model.Error;

/**
 * Contract that a View must implement in order to be considered as
 * a valid View for a Presenter
 *
 */
public interface ViewContract {

    /**
     * Shows a Loading indicator
     */
    void showLoadingIndicator();

    /**
     * Dismiss a Loading indicator
     */
    void dismissLoadingIndicator();

    /**
     * Shows an Error Message according to a well built
     * {@link Error}
     */
    void showErrorMessage(Error error);

    /**
     * Dismiss an Error message
     */
    void dismissErrorMessage();
}
