package com.elielmartinez.naturalfoodsstore.model;

import android.support.annotation.IdRes;

public class Error {
    private int errorCode;
    private @IdRes int errorTitle;
    private @IdRes int errorMessage;
}
