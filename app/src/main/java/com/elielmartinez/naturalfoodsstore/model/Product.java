package com.elielmartinez.naturalfoodsstore.model;

import com.google.gson.annotations.SerializedName;

public class Product {
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("price")
    private double price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return (obj != null) && (obj instanceof Product)
                && ((Product) obj).title != null
                && ((Product) obj).image != null
                && ((Product) obj).title.equals(this.title)
                && ((Product) obj).image.equals(this.image)
                && ((Product) obj).price == this.price;


    }

    @Override
    public int hashCode() {
        return (title + image + price).hashCode();
    }
}
